class Partie {

    private Joueur j1;
    private Joueur j2;

    public Partie(Joueur j1, Joueur j2) {
        this.j1=j1;
        this.j2=j2;
    }

    public Joueur getJ1() {
        return j1;
    }

    public void setJ1(Joueur j1) {
        this.j1 = j1;
    }

    public Joueur getj2() {
        return j2;
    }

    public void setj2(Joueur j2) {
        this.j2 = j2;
    }

    public String jouer(Joueur j1, Joueur j2) {
        if(j1.getChoix().matches("pierre|ciseaux|feuille") && j2.getChoix().matches("pierre|ciseaux|feuille")) {

            if(j1.getChoix().equals(j2.getChoix())) {
                return "Égalité";
            }else if(j1.getChoix().equals("pierre") && j2.getChoix().equals("ciseaux")) {
                return "Joueur 1 gagne";
            }else if(j1.getChoix().equals("ciseaux") && j2.getChoix().equals("feuille")) {
                return "Joueur 1 gagne";
            }else if(j1.getChoix().equals("feuille") && j2.getChoix().equals("pierre")) {
                return "Joueur 1 gagne";
            }
        return "Joueur 2 gagne";

        }

        return "j1 ou j2 n'a pas rentré un bon choix";
    }

}