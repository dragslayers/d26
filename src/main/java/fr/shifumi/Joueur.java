class Joueur {

    private String prenom;
    private String choix;

    public Joueur(String prenom, String choix) {
        this.prenom=prenom;
        this.choix=choix;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getChoix() {
        return choix;
    }

    public void setPrenom(String prenom) {
        this.prenom=prenom;
    }

    public void setChoix(String choix) {
        this.choix=choix;
    }
}