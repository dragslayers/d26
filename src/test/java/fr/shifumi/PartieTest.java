import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class PartieTest {

    private Joueur j1;
    private Joueur j2;
    private Partie partie;

    @BeforeEach
    public void init() {
        this.j1 = new Joueur("Martin", "ciseaux");
        this.j2 = new Joueur("Michel", "feuille");
        this.partie = new Partie(j1,j2);
    }

    @Test
    void j1_incorrect_input() {
        j1.setChoix("qdfghfdg");
        assertEquals("j1 ou j2 n'a pas rentré un bon choix", partie.jouer(j1,j2));
    }

    @Test
    void j2_incorrect_input() {
        j2.setChoix("qdfghfdg");
        assertEquals("j1 ou j2 n'a pas rentré un bon choix", partie.jouer(j1,j2));
    }

    @Test
    void should_draw_correctly() {
        j2.setChoix("ciseaux");
        assertEquals("Égalité", partie.jouer(j1,j2));
    }

    @Test
    void j1_should_win_correctly() {
        assertEquals("Joueur 1 gagne", partie.jouer(j1,j2));
    }

    @Test
    void j1_should_not_win() {
        j2.setChoix("ciseaux");
        assertNotEquals("Joueur 1 gagne", partie.jouer(j1,j2));
        j2.setChoix("pierre");
        assertNotEquals("Joueur 1 gagne", partie.jouer(j1,j2));
    }

    @Test
    void j2_should_win_correctly() {
        j2.setChoix("pierre");
        assertEquals("Joueur 2 gagne", partie.jouer(j1,j2));
    }

    @Test
    void j2_should_not_win() {
        j1.setChoix("feuille");
        assertNotEquals("Joueur 2 gagne", partie.jouer(j1,j2));
        j1.setChoix("ciseaux");
        assertNotEquals("Joueur 2 gagne", partie.jouer(j1,j2));
    }

    
}
